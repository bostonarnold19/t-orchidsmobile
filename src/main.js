import Vue from 'vue'
import axios from 'axios'
import Vue2Filters from 'vue2-filters'
import Framework7 from 'framework7'
import Framework7Vue from 'framework7-vue'
import Framework7Icon from 'framework7-icons/css/framework7-icons.css'
import Framework7Theme from 'framework7/dist/css/framework7.ios.min.css'
import Framework7ThemeColors from 'framework7/dist/css/framework7.ios.colors.min.css'
import AppStyles from './assets/sass/main.scss'
import Routes from './routes.js'
import App from './main.vue'

Vue.use(Vue2Filters)
Vue.use(Framework7Vue)

new Vue({
  el: '#app',
  template: '<app/>',
  framework7: {
    root: '#app',
    routes: Routes,
    cache: false
  },
  components: {
    app: App
  }
})

