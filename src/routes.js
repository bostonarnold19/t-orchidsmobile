export default [
  {
    path: '/stock',
    component: require('./assets/vue/stock/index.vue'),
  },
  {
    path: '/stock-create',
    component: require('./assets/vue/stock/create.vue'),
  },
  {
    path: '/stock-edit/stock/:stockId',
    component: require('./assets/vue/stock/edit.vue'),
  },
  {
    path: '/category',
    component: require('./assets/vue/category/index.vue'),
  },
  {
    path: '/category-create',
    component: require('./assets/vue/category/create.vue'),
  },
  {
    path: '/category-edit/category/:categoryId',
    component: require('./assets/vue/category/edit.vue'),
  },
  {
    path: '/product',
    component: require('./assets/vue/product/index.vue'),
  },
  {
    path: '/product-create',
    component: require('./assets/vue/product/create.vue'),
  },
  {
    path: '/product-edit/product/:productId',
    component: require('./assets/vue/product/edit.vue'),
  },
]
